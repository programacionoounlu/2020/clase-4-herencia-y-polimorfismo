import java.util.ArrayList;
import java.util.List;

public class Prueba {

	public static void main(String[] args) {
		Prenda prenda = new Prenda(100.00);		
		Remera remera = new Remera(100.00);
		Sweater sweater = new Sweater(100.00);
		Camisa camisa = new Camisa(100.00, true);
		Tarjeta tarjeta = new Tarjeta();
//		System.out.println("Precio de prenda $" + prenda.calcularPrecio());
//		System.out.println("Precio de remera $" + remera.calcularPrecio());
		ArrayList<Prenda> prendas = new ArrayList<Prenda>();
		prendas.add(camisa);
		prendas.add(remera);
		prendas.add(sweater);			
		prendas.add(prenda);
		for (Prenda prendaActual : prendas) {
			System.out.println("Precio de la prenda $ " + prendaActual.calcularPrecio());
			System.out.println("Precio de la prenda (con tarjeta) $ " + prendaActual.calcularPrecio(tarjeta));
			if(prendaActual instanceof Camisa) {
				Camisa camisa2 = (Camisa) prendaActual;
				if(camisa2.isMangaLarga()) {
					System.out.println("Es una camisa manga larga");
				}
			} else {
				System.out.println("Es otra prenda");
			}
		}
	}

}
