
public class Camisa extends Prenda{
	
	private boolean mangaLarga;

	public Camisa(Double precioDeLista, boolean mangaLarga) {
		super(precioDeLista);
		this.mangaLarga = mangaLarga;
	}

	public boolean isMangaLarga() {
		return this.mangaLarga;
	}
	
	@Override
	public Double calcularPrecio() {
		Double valorFinal = super.calcularPrecio();
		if(this.isMangaLarga()) {
			valorFinal += this.getPrecioDeLista() * 0.05;
		}
		return valorFinal;
	}
}
