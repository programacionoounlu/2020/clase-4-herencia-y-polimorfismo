
public class Sweater extends Prenda{

	public Sweater(Double precioDeLista) {
		super(precioDeLista);
	}
	
	@Override
	public Double calcularPrecio() {
		return this.getPrecioDeLista() * 1.08;
	}	
}
